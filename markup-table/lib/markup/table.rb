require "markup/table/cell_builder"
require "markup/table/row_builder"
require "markup/table/column_builder"
require "markup/table/table_builder"

module Markup
  module Table
    include Markup

    private

    def base_receivers
      [TableBuilder, ColumnBuilder, RowBuilder, CellBuilder] + super
    end
  end
end
