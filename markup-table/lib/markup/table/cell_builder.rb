module Markup
  module Table
    class CellBuilder < TagBuilder

      # @return [ActiveSupport::SafeBuffer] データセル要素
      def cell(*args, &block)
        tag(:td, *args, &block)
      end

      # @return [ActiveSupport::SafeBuffer] 見出しセル要素
      def head_cell(*args, &block)
        tag(:th, *args, &block)
      end

      private

      # @option options [String] :row 行名
      # @option options [String] :column 列名
      def tag(*args, **options, &block)
        options[:data] ||= {}
        %i(row column).each do |param|
          options[:data].merge!(param => options.delete(param)) if options.has_key?(param)
        end
        super(*args, options, &block)
      end
    end
  end
end
