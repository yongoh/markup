module Markup
  module Table
    class RowBuilder < TagBuilder

      # @param [String] row 行名
      # @param [Hash] default_html_attributes セル要素のHTML属性のデフォルト
      # @yield [m] 内容ブロック（必須）
      # @yieldparam [Markup::DomContainer] m セルビルダーとセルラッパーをレシーバとして持つDOMコンテナ
      # @return [ActiveSupport::SafeBuffer] セル群
      def cells(row: nil, **default_html_attributes, &block)
        default_html_attributes = default_html_attributes.html_attribute_merge(data: {row: row}) if row
        builder = CellBuilder.new(agent, nil, default_html_attributes)
        wrapper = Wrapper.new(agent, &builder.method(:cell))
        DomContainer.markup(builder, wrapper, &block)
      end

      # @return [ActiveSupport::SafeBuffer] 行要素
      def row(*args, &block)
        tag(:tr, *args, &block)
      end

      private

      # @param [String] row 行名
      def tag(*args, row: nil, **options, &block)
        options = options.html_attribute_merge(data: {row: row}) if row
        content_proc = ->(m){ m << cells(row: row, &block) } if block_given?
        super(*args, **options, &content_proc)
      end
    end
  end
end
