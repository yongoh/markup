module Markup
  module Table
    class ColumnBuilder < TagBuilder

      # @return [ActiveSupport::SafeBuffer] 列要素
      def column(**options)
        tag(:col, options)
      end

      # @param [String] column 列名
      # @param [Hash] default_html_attributes 列要素のHTML属性のデフォルト
      # @yield [m] 内容ブロック（必須）
      # @yieldparam [Markup::DomContainer] m 列ビルダーを唯一のレシーバとして持つDOMコンテナ
      # @return [ActiveSupport::SafeBuffer] 列要素群
      def columns(column: nil, **default_html_attributes, &block)
        default_html_attributes = default_html_attributes.html_attribute_merge(data: {column: column}) if column
        builder = ColumnBuilder.new(agent, nil, default_html_attributes)
        DomContainer.markup(builder, &block)
      end

      # @return [ActiveSupport::SafeBuffer] 列グループ要素
      def column_group(*args, &block)
        tag(:colgroup, *args, &block)
      end

      private

      # @param [String] column 列名
      def tag(*args, column: nil, **options, &block)
        options = options.html_attribute_merge(data: {column: column}) if column
        content_proc = ->(m){ m << columns(column: column, &block) } if block_given?
        super(*args, **options, &content_proc)
      end
    end
  end
end
