module Markup
  module Table
    class TableBuilder < TagBuilder

      delegate :column, :column_group, to: :column_builder

      # @return [ActiveSupport::SafeBuffer] キャプション要素
      def caption(*args, &block)
        tag(:caption, *args, &block)
      end

      # @param [Hash] default_html_attributes 行要素のHTML属性のデフォルト
      # @yield [m] 内容ブロック（必須）
      # @yieldparam [Markup::DomContainer] m 行ビルダーと行ラッパーをレシーバとして持つDOMコンテナ
      # @return [ActiveSupport::SafeBuffer] 行群
      def rows(**default_html_attributes, &block)
        builder = RowBuilder.new(agent, nil, default_html_attributes)
        wrapper = Wrapper.new(agent, &builder.method(:row))
        DomContainer.markup(builder, wrapper, &block)
      end

      %i(thead tbody tfoot).each do |tag_name|

        # @return [ActiveSupport::SafeBuffer] 行グループ要素
        define_method(tag_name) do |*args, &block|
          content_proc = ->(m){ m << rows(&block) } if block
          tag(tag_name, *args, &content_proc)
        end
      end

      private

      def column_builder
        @column_builder ||= ColumnBuilder.new(agent)
      end
    end
  end
end
