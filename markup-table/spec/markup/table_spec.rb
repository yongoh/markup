require 'spec_helper'

describe Markup::Table do
  let(:receiver){ double("Receiver", shobon: "(´･ω･｀)", boon: "（＾ω＾）") }

  describe "#markup" do
    let(:owner){ double("Owner").extend(described_class) }
    let(:result){ owner.markup(receiver) }

    shared_examples_for "agent include table builders" do
      it "は、レシーバ群に表系ビルダー群を含めること" do
        is_expected.to match([
          be_instance_of(Markup::Table::TableBuilder),
          be_instance_of(Markup::Table::ColumnBuilder),
          be_instance_of(Markup::Table::RowBuilder),
          be_instance_of(Markup::Table::CellBuilder),
          be_a(Markup::HtmlBuilder),
          be_a(ActionView::Base),
        ])
      end
    end

    describe "yieldparam._agent" do
      subject{ owner.markup{|m| return m._agent.receivers } }
      it_behaves_like "agent include table builders"
    end

    describe "result._agent" do
      subject{ owner.markup._agent.receivers }
      it_behaves_like "agent include table builders"
    end

    describe ".cell" do
      it{ expect(result.cell(:shobon)).to have_tag("td", text: "(´･ω･｀)") }
    end

    describe ".row" do
      it{ expect(result.row(:shobon)).to have_tag("tr", text: "(´･ω･｀)") }
    end

    describe ".column" do
      it{ expect(result.column(span: 2)).to have_tag("col:empty", with: {span: 2}) }
    end

    describe ".thead" do
      subject do
        result.thead do |m|
          m.row(:shobon)
          m.row do |m|
            m.cell(:shobon)
          end
        end
      end

      example do
        is_expected.to have_tag("thead"){
          with_tag("tr", text: "(´･ω･｀)")
          with_tag("tr"){
            with_tag("td", text: "(´･ω･｀)")
          }
        }
      end
    end
  end
end
