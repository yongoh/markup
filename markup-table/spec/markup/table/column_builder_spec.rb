require 'spec_helper'

describe Markup::Table::ColumnBuilder do
  let(:builder){ described_class.new(agent) }
  let(:agent){ Markup::Agent.new(receiver) }
  let(:receiver){ double("Receiver") }

  describe "#column" do
    context "引数なしの場合" do
      it "は、列要素を生成すること" do
        expect(builder.column).to have_tag("col:not([data-column]):empty")
      end
    end

    context "第1引数にHTML属性を渡した場合" do
      it "は、渡したHTML属性を持つ列要素を生成すること" do
        expect(builder.column(id: "my-id", class: "my-class")).to have_tag("col:not([data-column]):empty.my-class#my-id")
      end
    end

    context "第1引数に内容を渡した場合" do
      it{ expect{ builder.column("列") }.to raise_error(ArgumentError, "wrong number of arguments (given 1, expected 0)") }
    end

    context "オプション`:column`に列名を渡した場合" do
      it "は、データ属性に列名を持つ列要素を生成すること" do
        expect(builder.column(column: "column-1")).to have_tag("col:empty", with: {"data-column" => "column-1"})
      end
    end
  end

  describe "#columns" do
    describe "ブロック第1引数" do
      let(:container){ builder.columns{|m| return m } }

      it "は、列ビルダーを唯一のレシーバとして持つ持つDOMコンテナを渡すこと" do
        expect(container).to be_instance_of(Markup::DomContainer)
        expect(container._agent.receivers).to match([be_instance_of(Markup::Table::ColumnBuilder)])
      end
    end

    context "ブロックを渡さない場合" do
      it "は、ブロックが無いことを示すエラーを発生させること" do
        expect{ builder.columns }.to raise_error(LocalJumpError, "no block given (yield)")
      end
    end

    context "ブロック内で列要素群を作成した場合" do
      subject do
        builder.columns do |m|
          m.column(span: 1)
          m.column(span: 2)
        end
      end

      it "は、列要素群を生成すること" do
        is_expected.to have_tag("col:empty", with: {span: "1"}, count: 1)
        is_expected.to have_tag("col:empty", with: {span: "2"}, count: 1)
      end
    end
  end

  describe "#column_group" do
    context "引数なしの場合" do
      subject do
        builder.column_group do |m|
          m.column(span: 1)
          m.column(span: 2)
        end
      end

      it "は、列グループ要素を生成すること" do
        is_expected.to have_tag("colgroup:not([data-column])", count: 1){
          is_expected.to have_tag("col:not([data-column]):empty", with: {span: "1"}, count: 1)
          is_expected.to have_tag("col:not([data-column]):empty", with: {span: "2"}, count: 1)
        }
      end
    end

    context "第1引数に内容を渡した場合" do
      it "は、渡した内容を持つ列グループ要素を生成すること" do
        expect(builder.column_group("れつ")).to have_tag("colgroup:not([data-column])", text: "れつ", count: 1)
      end
    end

    context "第1引数にHTML属性を渡した場合" do
      subject do
        builder.column_group(class: "my-class") do |m|
          m.column(span: 1)
          m.column(span: 2)
        end
      end

      it "は、渡したHTML属性を持つ列グループ要素を生成すること" do
        is_expected.to have_tag("colgroup:not([data-column]).my-class", count: 1){
          is_expected.to have_tag("col:not([data-column]):empty:not(.my-class)", with: {span: "1"}, count: 1)
          is_expected.to have_tag("col:not([data-column]):empty:not(.my-class)", with: {span: "2"}, count: 1)
        }
      end
    end

    context "オプション`:column`に列名を渡した場合" do
      subject do
        builder.column_group(column: "column-1") do |m|
          m.column(span: 1)
          m.column(span: 2)
        end
      end

      it "は、列グループ要素と配下の列要素のデータ属性に列名を持たせること" do
        is_expected.to have_tag("colgroup", with: {"data-column" => "column-1"}, count: 1){
          is_expected.to have_tag("col:empty", with: {"data-column" => "column-1", span: "1"}, count: 1)
          is_expected.to have_tag("col:empty", with: {"data-column" => "column-1", span: "2"}, count: 1)
        }
      end
    end
  end
end
