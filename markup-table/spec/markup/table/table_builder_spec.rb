require 'spec_helper'

describe Markup::Table::TableBuilder do
  let(:builder){ described_class.new(agent) }
  let(:agent){ Markup::Agent.new(receiver) }
  let(:receiver){
    cells = h.capture do
      h.concat h.content_tag(:th, "みだし")
      h.concat h.content_tag(:td, "でーた")
    end
    double("Receiver", shobon: "(´･ω･｀)", boon: "（＾ω＾）", xxx_cells: cells)
  }

  describe "#column" do
    it "は、列要素を生成すること" do
      expect(builder.column).to have_tag("col:empty")
    end
  end

  describe "#column_group" do
    subject do
      builder.column_group do |m|
        m.column
        m.column
      end
    end

    it "は、列グループ要素を生成すること" do
      is_expected.to have_tag("colgroup"){
        is_expected.to have_tag("col:empty", count: 2)
      }
    end
  end

  describe "#caption" do
    subject do
      builder.caption(class: "shobon") do |m|
        m.shobon
        m << "しょぼ～ん"
      end
    end

    it "は、<caption>を生成すること" do
      is_expected.to have_tag("caption.shobon", text: "(´･ω･｀)しょぼ～ん")
    end
  end

  describe "#rows" do
    describe "ブロック第1引数" do
      let(:container){ builder.rows{|m| return m } }

      it "は、行ビルダーと行ラッパーをレシーバとして持つ持つDOMコンテナを渡すこと" do
        expect(container).to be_instance_of(Markup::DomContainer)
        expect(container._agent.receivers).to match([
          be_instance_of(Markup::Table::RowBuilder),
          be_instance_of(Markup::Wrapper).and(have_attributes(_agent: equal(agent))),
        ])
      end

      describe "レシーバが持つメソッド名 + '!'" do
        it "は、レシーバに移譲した結果を内容に持つ行を生成すること" do
          expect(container.shobon!).to have_tag("tr", text: "(´･ω･｀)")
        end
      end
    end

    context "ブロックを渡さない場合" do
      it "は、ブロックが無いことを示すエラーを発生させること" do
        expect{ builder.rows }.to raise_error(LocalJumpError, "no block given (yield)")
      end
    end

    context "ブロック内で行群を作成した場合" do
      subject do
        builder.rows do |m|
          m.row do |m|
            m.head_cell(:shobon)
          end
          m.row do |m|
            m.cell(:boon)
          end
          m.xxx_cells
        end
      end

      it "は、行群を生成すること" do
        is_expected.to have_tag("tr"){
          with_tag("th", text: "(´･ω･｀)")
        }
        is_expected.to have_tag("tr"){
          with_tag("td", text: "（＾ω＾）")
        }
        is_expected.to have_tag("tr"){
          with_tag("th", text: "みだし")
          with_tag("td", text: "でーた")
        }
      end
    end
  end

  describe "#thead" do
    context "引数で内容とHTML属性を渡した場合" do
      subject do
        builder.thead(h.tag(:tr, class: "row1"), id: "my-id")
      end

      it "は、内容とHTML属性を持つ<thead>を生成すること" do
        is_expected.to have_tag("thead#my-id"){
          with_tag("tr.row1")
        }
      end
    end

    context "ブロックを渡した場合" do
      subject do
        builder.thead(id: "my-id") do |m|
          m.row do |m|
            m.head_cell(:shobon)
          end
          m.row do |m|
            m.cell(:boon)
          end
          m.xxx_cells
        end
      end

      it "は、ブロック内で作成した行群を含む<thead>を生成すること" do
        is_expected.to have_tag("thead#my-id"){
          with_tag("tr:not(#my-id)"){
            with_tag("th:not(#my-id)", text: "(´･ω･｀)")
          }
          with_tag("tr:not(#my-id)"){
            with_tag("td:not(#my-id)", text: "（＾ω＾）")
          }
          with_tag("tr:not(#my-id)"){
            with_tag("th:not(#my-id)", text: "みだし")
            with_tag("td:not(#my-id)", text: "でーた")
          }
        }
      end
    end
  end
end
