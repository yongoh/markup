require 'spec_helper'

describe Markup::Table::RowBuilder do
  let(:builder){ described_class.new(agent) }
  let(:agent){ Markup::Agent.new(receiver) }
  let(:receiver){ double("Receiver", shobon: "(´･ω･｀)", boon: "（＾ω＾）") }

  before do
    allow(receiver).to receive(:abc){|a, b, &c| "A=#{a},B=#{b},C=#{c.call}" }
  end

  describe "#cells" do
    describe "ブロック第1引数" do
      let(:container){ builder.cells{|m| return m } }

      it "は、セルビルダーとセルラッパーをレシーバとして持つ持つDOMコンテナを渡すこと" do
        expect(container).to be_instance_of(Markup::DomContainer)
        expect(container._agent.receivers).to match([
          be_instance_of(Markup::Table::CellBuilder),
          be_instance_of(Markup::Wrapper).and(have_attributes(_agent: equal(agent))),
        ])
      end

      describe "レシーバが持つメソッド名 + '!'" do
        it "は、レシーバに移譲した結果を内容に持つセルを生成すること" do
          expect(container.shobon!).to have_tag("td", text: "(´･ω･｀)")
        end
      end
    end

    context "ブロックを渡さない場合" do
      it "は、ブロックが無いことを示すエラーを発生させること" do
        expect{ builder.cells }.to raise_error(LocalJumpError, "no block given (yield)")
      end
    end

    context "ブロック内でセル群を作成した場合" do
      subject do
        builder.cells do |m|
          m.head_cell(:shobon)
          m.cell(:boon)
          m.shobon
        end
      end

      it "は、セル群を生成すること" do
        is_expected.to have_tag("th", text: "(´･ω･｀)", count: 1)
        is_expected.to have_tag("td", text: "（＾ω＾）", count: 1)
        is_expected.to have_tag("td", text: "(´･ω･｀)", count: 1)
      end
    end
  end

  describe "#row" do
    context "第1引数に内容を渡した場合" do
      it "は、渡した内容を持つ行要素を生成すること" do
        expect(builder.row("ぎょう")).to have_tag("tr:not([data-row])", text: "ぎょう")
      end
    end

    context "第1引数にSymbolを渡した場合" do
      it "は、その名前のメソッドを移譲した結果を内容として持つ行要素を生成すること" do
        expect(builder.row(:shobon)).to have_tag("tr:not([data-row])", text: "(´･ω･｀)")
      end
    end

    context "オプション`:row`に行名を渡した場合" do
      it "は、データ属性に行名を持つ行要素を生成すること" do
        expect(builder.row(row: "row-1")).to have_tag("tr:empty", with: {"data-row" => "row-1"})
      end
    end

    context "ブロックを渡した場合" do
      context "引数を渡さない場合" do
        subject do
          builder.row do |m|
            m.head_cell(:shobon)
            m.cell(:boon)
            m.cell("ほげ")
            Markup.h.concat h.content_tag("td", "ぴよ")
          end
        end

        it "は、ブロック内で生成したセル群を含む行要素を返すこと" do
          is_expected.to have_tag("tr:not([data-row])"){
            with_tag("td:not([data-row])", text: "ぴよ", count: 1)
            with_tag("th:not([data-row])", text: "(´･ω･｀)", count: 1)
            with_tag("td:not([data-row])", text: "（＾ω＾）", count: 1)
            with_tag("td:not([data-row])", text: "ほげ", count: 1)
          }
        end
      end

      context "HTML属性を渡した場合" do
        subject do
          builder.row(class: "my-class") do |m|
            m.head_cell(:shobon)
            m.cell(:boon)
            m.cell("ほげ")
            Markup.h.concat h.content_tag("td", "ぴよ")
          end
        end

        it "は、行要素にのみ渡したHTML属性を付加すること" do
          is_expected.to have_tag("tr:not([data-row]).my-class"){
            with_tag("td:not([data-row]):not(.my-class)", text: "ぴよ", count: 1)
            with_tag("th:not([data-row]):not(.my-class)", text: "(´･ω･｀)", count: 1)
            with_tag("td:not([data-row]):not(.my-class)", text: "（＾ω＾）", count: 1)
            with_tag("td:not([data-row]):not(.my-class)", text: "ほげ", count: 1)
          }
        end
      end

      context "オプション`:row`に行名を渡した場合" do
        subject do
          builder.row(row: "row-1") do |m|
            m.head_cell(:shobon)
            m.cell(:boon)
            m.cell("ほげ")
            Markup.h.concat h.content_tag("td", "ぴよ")
          end
        end

        it "は、行要素と配下のセル要素のデータ属性に行名を持たせること" do
          is_expected.to have_tag("tr", with: {"data-row" => "row-1"}){
            with_tag("td:not([data-row])", text: "ぴよ", count: 1)
            with_tag("th", with: {"data-row" => "row-1"}, text: "(´･ω･｀)", count: 1)
            with_tag("td", with: {"data-row" => "row-1"}, text: "（＾ω＾）", count: 1)
            with_tag("td", with: {"data-row" => "row-1"}, text: "ほげ", count: 1)
          }
        end
      end

      context "インスタンス化時にデフォルト値を渡した場合" do
        let(:builder){ described_class.new(receiver, "(・∀・)", class: "default-class", id: "default-id") }

        subject do
          builder.row(class: "my-class", id: "my-id")
        end

        it "は、マージしたHTML属性を持つ行要素を生成すること" do
          is_expected.to have_tag("tr:not([data-row]).my-class#my-id", text: "(・∀・)")
        end
      end
    end
  end
end
