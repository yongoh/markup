require 'spec_helper'

describe Markup::Table::CellBuilder do
  let(:builder){ described_class.new(agent) }
  let(:agent){ Markup::Agent.new(receiver) }
  let(:receiver){ double("Receiver", shobon: "(´･ω･｀)") }

  before do
    allow(receiver).to receive(:abc){|a, b, &c| "A=#{a},B=#{b},C=#{c.call}" }
  end

  describe "#cell" do
    context "オプションを渡さない場合" do
      it "は、データセル要素を生成すること" do
        expect(builder.cell("ないよう")).to have_tag("td:not([data-column]):not([data-row])", text: "ないよう")
      end
    end

    context "オプション`:row`に行名を渡した場合" do
      it "は、データ属性に行名を持つデータセル要素を生成すること" do
        expect(builder.cell("ないよう", row: "row-1")).to have_tag("td:not([data-column])", with: {"data-row" => "row-1"}, text: "ないよう")
      end
    end

    context "オプション`:column`に列名を渡した場合" do
      it "は、データ属性に列名を持つデータセル要素を生成すること" do
        expect(builder.cell("ないよう", column: "column-1")).to have_tag("td:not([data-row])", with: {"data-column" => "column-1"}, text: "ないよう")
      end
    end
  end

  describe "#head_cell" do
    it "は、見出しセルを生成すること" do
      expect(builder.head_cell("みだし")).to have_tag("th:not([data-column])", text: "みだし")
    end
  end
end
