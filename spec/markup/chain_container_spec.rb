require 'spec_helper'

describe Markup::ChainContainer do
  let(:chain_container){ described_class.new(agent, []) }
  let(:agent){ Markup::Agent.new(receiver) }
  let(:receiver){ double("Receiver", hoge: "ほげ") }

  describe "#_order!" do
    it "は、`@_agent.receive`に移譲した結果を返すこと" do
      expect(chain_container._order!(:hoge)).to eq("ほげ")
    end

    it "は、`@_agent.receive`に移譲した結果を`@_doms`に追加しないこと" do
      chain_container._order!(:hoge)
      expect(chain_container._doms).not_to include("ほげ")
    end

    context "`@_agent`が持つレシーバが`@_doms`に含まれる場合" do
      let(:chain_container){ described_class.new(agent, [*%w(あ い う え お), receiver]) }

      it "は、`@_doms`からレシーバを削除すること" do
        chain_container._order!(:hoge)
        expect(chain_container._doms).to match(%w(あ い う え お))
        expect(chain_container._doms).not_to include(agent)
      end
    end
  end
end
