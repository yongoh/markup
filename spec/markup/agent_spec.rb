require 'spec_helper'

describe Markup::Agent do
  let(:agent){ described_class.new(receiver_0, receiver_1, receiver_2) }

  let(:receiver_0){ double("Receiver0", hoge: "Hoge!") }
  let(:receiver_1){ double("Receiver1", hoge: "Hoge?", piyo: "Piyo?") }
  let(:receiver_2){ double("Receiver2", piyo: "Piyo!", foobar: "FooBar") }
  let(:receiver_3){ double("Receiver3") }
  let(:receiver_4){ double("Receiver4") }

  describe "#push" do
    it "は、レシーバを`@receivers`の末尾に格納すること" do
      agent.push(receiver_3, receiver_4)
      expect(agent.receivers).to match([receiver_0, receiver_1, receiver_2, receiver_3, receiver_4])
    end

    it "は、自身を返すこと" do
      expect(agent.push(receiver_3)).to equal(agent)
    end

    context "DOMビルダークラスを渡した場合" do
      let(:builder_class){ Class.new(Markup::DomBuilder) }

      it "は、自身を渡してインスタンス化したものを`@receivers`の末尾に格納すること" do
        agent.push(builder_class)
        expect(agent.receivers.last).to be_instance_of(builder_class).and have_attributes(agent: equal(agent))
      end
    end

    context "それ以外のクラスを渡した場合" do
      let(:klass){ Class.new }

      it "は、渡したクラスを`@receivers`の末尾に格納すること" do
        agent.push(klass)
        expect(agent.receivers).to match([receiver_0, receiver_1, receiver_2, klass])
      end
    end

    context "別のエージェントオブジェクトを渡した場合" do
      let(:other){ described_class.new(receiver_3, receiver_4) }

      it "は、渡したエージェントのレシーバ群を`@receivers`の末尾に格納すること" do
        agent.push(other)
        expect(agent.receivers).to match([receiver_0, receiver_1, receiver_2, receiver_3, receiver_4])
      end
    end

    context "配列を渡した場合" do
      it "は、配列を`@receivers`の末尾に格納すること" do
        agent.push([receiver_3, receiver_4])
        expect(agent.receivers).to match([receiver_0, receiver_1, receiver_2, [receiver_3, receiver_4]])
      end
    end

    context "既に含まれているレシーバを渡した場合" do
      it "は、重複を除外して`@receivers`の末尾に格納すること" do
        agent.push(receiver_2, receiver_3)
        expect(agent.receivers).to match([receiver_0, receiver_1, receiver_2, receiver_3])
      end
    end
  end

  describe "#<<" do
    it "は、レシーバを`@receivers`の末尾に格納すること" do
      agent << receiver_3
      expect(agent.receivers).to match([receiver_0, receiver_1, receiver_2, receiver_3])
    end

    it "は、自身を返すこと" do
      expect(agent << receiver_3).to equal(agent)
    end
  end

  describe "#unshift" do
    it "は、レシーバを`@receivers`の先頭に格納すること" do
      agent.unshift(receiver_3, receiver_4)
      expect(agent.receivers).to match([receiver_3, receiver_4, receiver_0, receiver_1, receiver_2])
    end

    it "は、自身を返すこと" do
      expect(agent.unshift(receiver_3)).to equal(agent)
    end
  end

  describe "#concat" do
    it "は、レシーバ群を`@receivers`の末尾に格納すること" do
      agent.concat([receiver_3, receiver_4])
      expect(agent.receivers).to match([receiver_0, receiver_1, receiver_2, receiver_3, receiver_4])
    end

    it "は、自身を返すこと" do
      expect(agent.concat([receiver_3])).to equal(agent)
    end
  end

  describe "#delete" do
    it "は、`@receivers`から引数と同じオブジェクトを削除すること" do
      agent.delete(receiver_1)
      expect(agent.receivers).to match([receiver_0, receiver_2])
    end

    it "は、引数を返すこと" do
      expect(agent.delete(receiver_1)).to equal(receiver_1)
    end
  end

  describe "#+" do
    subject do
      agent + other
    end

    shared_examples_for "plus" do
      it "は、レシーバ群を連結したエージェントを返すこと" do
        is_expected.to be_instance_of(Markup::Agent).and have_attributes(receivers: [receiver_0, receiver_1, receiver_2, receiver_3, receiver_4])
      end

      it{ is_expected.not_to equal(agent) }
    end

    context "他のエージェントを渡した場合" do
      let(:other){ Markup::Agent.new(receiver_3, receiver_4) }
      it_behaves_like "plus"
    end

    context "レシーバの配列を渡した場合" do
      let(:other){ [receiver_3, receiver_4] }
      it_behaves_like "plus"
    end
  end

  describe "#-" do
    subject do
      agent - other
    end

    shared_examples_for "minus" do
      it "は、レシーバ群を連結したエージェントを返すこと" do
        is_expected.to be_instance_of(Markup::Agent).and have_attributes(receivers: [receiver_2])
      end

      it{ is_expected.not_to equal(agent) }
    end

    context "他のエージェントを渡した場合" do
      let(:other){ Markup::Agent.new(receiver_0, receiver_1) }
      it_behaves_like "minus"
    end

    context "レシーバの配列を渡した場合" do
      let(:other){ [receiver_0, receiver_1] }
      it_behaves_like "minus"
    end
  end

  describe "#receive" do
    context "特定のレシーバだけが持つメソッド名を渡した場合" do
      it "は、そのレシーバに移譲した結果を返すこと" do
        expect(agent.receive(:foobar)).to eq("FooBar")
      end
    end

    context "複数のレシーバが持つメソッド名を渡した場合" do
      it "は、メソッドを持つレシーバのうちハッシュに含まれる順番が最初のものに移譲すること" do
        expect(agent.receive(:piyo)).to eq("Piyo?")
      end
    end

    context "どのレシーバにも存在しないメソッド名を渡した場合" do
      it "は、メソッド未定義エラーを返すこと" do
        expect{ agent.receive(:undefined_method) }.to raise_error(NoMethodError, "undefined method `undefined_method' for [#<Double \"Receiver0\">, #<Double \"Receiver1\">, #<Double \"Receiver2\">]")
      end

      context "`#inspect`が長い文字列を返すレシーバを含む場合" do
        before do
          allow(receiver_1).to receive(:inspect).and_return(inspect)
        end

        let(:inspect){ "#<Receiver \"#{"abcdefghij" * 600}\">" }

        it "は、省略されたinspectをメッセージに含むメソッド未定義エラーを返すこと" do
          expect{ agent.receive(:undefined_method) }.to raise_error(NoMethodError, "undefined method `undefined_method' for [#<Double \"Receiver0\">, #{inspect.slice(0, 500)}..., #<Double \"Receiver2\">]")
        end
      end
    end

    context "メソッド名と引数を渡した場合" do
      before do
        h = view_context
        allow(receiver_0).to receive(:method_2){|*args| h.content_tag(:span, *args) }
      end

      subject do
        agent.receive(:method_2, "(´･ω･｀)", id: "shobon")
      end

      it "は、引数付きで移譲した結果を返すこと" do
        is_expected.to have_tag("span#shobon", text: "(´･ω･｀)")
      end
    end
  end

  describe "#responded_receivers_to" do
    it "は、渡したメソッド名に応答可能なレシーバ群を返すこと" do
      expect(agent.responded_receivers_to(:piyo)).to match([receiver_1, receiver_2])
    end
  end

  describe "#responded_receiver_to" do
    context "応答可能なレシーバが1つ以上ある場合" do
      it "は、渡したメソッド名に応答可能なレシーバのうち最初のものを返すこと" do
        expect(agent.responded_receiver_to(:piyo)).to equal(receiver_1)
      end
    end

    context "応答可能なレシーバが無い場合" do
      it{ expect(agent.responded_receiver_to(:undefined_method)).to be_nil }
    end
  end

  describe "#receivers_respond_to?" do
    context "応答可能なレシーバが1つ以上ある場合" do
      it{ expect(agent).to be_receivers_respond_to(:piyo) }
    end

    context "応答可能なレシーバが無い場合" do
      it{ expect(agent).not_to be_receivers_respond_to(:undefined_method) }
    end
  end
end
