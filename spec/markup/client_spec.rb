require 'spec_helper'

describe Markup::Client do
  let(:client){ described_class.new(agent) }
  let(:agent){ Markup::Agent.new(receiver) }
  let(:receiver){ double("Receiver", hoge: "ほげ") }

  describe "#_order" do
    context "メソッド名のみを渡した場合" do
      it "は、`@_agent.receive`に移譲した結果を返すこと" do
        expect(client._order(:hoge)).to eq("ほげ")
      end
    end

    context "引数とブロックを渡した場合" do
      before do
        allow(receiver).to receive(:div){|html_attributes, &block| h.content_tag("div", block.call, html_attributes) }
      end

      it "は、`@_agent.receive`に移譲した結果を返すこと" do
        expect(client._order(:div, class: "shobon"){ "(´･ω･｀)" }).to have_tag("div.shobon", text: "(´･ω･｀)")
      end
    end
  end

  describe "#respond_to?" do
    context "自身が持つメソッド名を渡した場合" do
      it{ expect(client).to be_respond_to(:_agent) }
    end

    context "レシーバが持つメソッド名を渡した場合" do
      it{ expect(client).to be_respond_to(:hoge) }
    end

    context "自身もレシーバも持たないメソッド名を渡した場合" do
      it{ expect(client).not_to be_respond_to(:unknown) }
    end
  end

  describe "#method_missing (private method)" do
    it "は、`#_order`に移譲すること" do
      expect(client.hoge).to eq("ほげ")
    end
  end
end
