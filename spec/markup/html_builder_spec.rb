require 'spec_helper'

describe Markup::HtmlBuilder do
  let(:builder){ described_class.new(agent) }
  let(:agent){ double("Agent") }

  describe "#span（`#tag`に移譲）" do
    context "第1引数に内容・第2引数にHTML属性を渡した場合" do
      subject do
        builder.span("(´･ω･｀)", class: "shobon")
      end

      it "は、内容と属性を持つタグを生成すること" do
        should have_tag("span.shobon", text: "(´･ω･｀)")
      end
    end
  end
end
