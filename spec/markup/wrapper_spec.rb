require 'spec_helper'

describe Markup::Wrapper do
  let(:wrapper){ described_class.new(agent){|result| result.sub("ω", "Д") } }
  let(:agent){ Markup::Agent.new(receiver) }
  let(:receiver){ double("Receiver", shobon: "(´･ω･｀)") }

  describe "#_order" do
    it "は、レシーバに移譲した結果をブロックで処理した結果を返すこと" do
      expect(wrapper._order(:shobon)).to eq("(´･Д･｀)")
    end
  end

  describe "#method_missing (private method)" do
    it "は、レシーバに移譲した結果をブロックで処理した結果を返すこと" do
      expect(wrapper.shobon).to eq("(´･Д･｀)")
    end
  end
end
