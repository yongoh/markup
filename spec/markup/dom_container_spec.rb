require 'spec_helper'

describe Markup::DomContainer do
  let(:dom_container){ described_class.new(agent) }
  let(:agent){ Markup::Agent.new(receiver_0) }
  let(:receiver_0){ double("Receiver0", hoge: "ほげ", piyo: "ぴよ") }
  let(:receiver_1){ double("Receiver1", hoge: "HOGE!") }

  describe "::markup" do
    context "ブロック内で`Markup.view_context.concat`を使ってDOMを渡した場合" do
      subject do
        described_class.markup do
          Markup.h.concat h.content_tag("div", "(´･ω･｀)", id: "shobon")
          Markup.h.concat "てきすと"
          Markup.h.concat h.content_tag("span", "(・∀・)", class: "kao")
        end
      end

      it "は、`concat`した要素を含む要素群を返すこと" do
        is_expected.to be_instance_of(ActiveSupport::SafeBuffer)
        is_expected.to have_tag("div#shobon", text: "(´･ω･｀)", count: 1)
        is_expected.to match("てきすと")
        is_expected.to have_tag("span.kao", text: "(・∀・)", count: 1)
      end
    end

    context "ブロック内で`Markup::view_context`以外のコンテキストの`#concat`を使ってDOMを渡した場合" do
      it "は、エラーを発生させること" do
        expect{
          described_class.markup do
            h.concat "てきすと"
          end
        }.to raise_error(NoMethodError)
      end
    end

    context "レシーバを渡し、ブロック内でDOMコンテナを使ってDOMを追加した場合" do
      subject do
        described_class.markup(receiver) do |m|
          m.naku
          m << "テキスト"
          m.okoru
        end
      end

      let(:receiver){
        double("Receiver",
          naku: h.content_tag("div", "(つД｀)", id: "naku"),
          okoru: h.content_tag("span", "ヽ(`Д´)ﾉ", class: "okoru"),
        )
      }

      it "は、DOMコンテナに追加した要素を含む要素群を返すこと" do
        is_expected.to be_instance_of(ActiveSupport::SafeBuffer)
        is_expected.to have_tag("div#naku", text: "(つД｀)", count: 1)
        is_expected.to match("テキスト")
        is_expected.to have_tag("span.okoru", text: "ヽ(`Д´)ﾉ", count: 1)
      end
    end

    context "ブロック内で`ActionView::Base#concat`とDOMコンテナを両方使ってDOMを追加した場合" do
      subject do
        described_class.markup do |m|
          m << "テキスト"
          Markup.h.concat "てきすと"
        end
      end

      it "は、`concat`で追加したDOMを先に並べた要素群を返すこと" do
        is_expected.to be_instance_of(ActiveSupport::SafeBuffer)
        is_expected.to eq("てきすとテキスト")
      end
    end

    describe "ブロックの戻り値" do
      it "は、無視されること" do
        expect(described_class.markup{ "result" }).to be_nil
      end
    end
  end

  describe "#_order!" do
    it "は、`@_agent.receive`に移譲した結果を返すこと" do
      expect(dom_container._order!(:hoge)).to eq("ほげ")
    end

    it "は、`@_agent.receive`に移譲した結果を`@_doms`に追加しないこと" do
      dom_container._order!(:hoge)
      expect(dom_container._doms).not_to include("ほげ")
    end
  end

  describe "#_order" do
    it "は、`@_agent.receive`に移譲した結果をレシーバに持つメソッドチェーン用DOMコンテナオブジェクトを返すこと" do
      result = dom_container._order(:hoge)

      expect(result).to be_instance_of(Markup::ChainContainer)
      expect(result._agent.receivers).to match(["ほげ"])
      expect(result._doms).to equal(dom_container._doms)
    end

    it "は、`@_agent.receive`に移譲した結果を`@_doms`の最後に追加すること" do
      dom_container._doms.concat(%w(あ い う え お))
      dom_container._order(:hoge)
      expect(dom_container._doms.last).to eq("ほげ")
    end
  end

  describe "#method_missing (private method)" do
    context "'!'付きメソッド名で呼び出した場合" do
      it "は、`#_order!`に移譲すること" do
        expect(dom_container.hoge!).to eq("ほげ")
        expect(dom_container._doms).not_to include("ほげ")
      end
    end

    context "'!'無しメソッド名で呼び出した場合" do
      it "は、`#_order`に移譲すること" do
        dom_container._doms.concat(%w(あ い う え お))
        dom_container.hoge
        expect(dom_container._doms.last).to eq("ほげ")
      end
    end

    context "メソッドチェーンで呼び出した場合" do
      before do
        allow(receiver_0).to receive(:foo).and_return(foo)
        allow(baz).to receive_message_chain(:class, :name).and_return("Baz")
      end

      let(:foo){ double("Foo", bar: bar) }
      let(:bar){ double("Bar", baz: baz) }
      let(:baz){ double("Baz") }

      it "は、最後のメソッドの結果をレシーバに持つメソッドチェーン用DOMコンテナオブジェクトを返すこと" do
        result = dom_container.foo.bar.baz

        expect(result).to be_instance_of(Markup::ChainContainer)
        expect(result._agent.receivers).to match([baz])
        expect(result._doms).to equal(dom_container._doms)
      end

      it "は、最後のメソッドの結果のみを`@_doms`に追加すること" do
        dom_container.foo.bar.baz
        expect(dom_container._doms).not_to include(foo)
        expect(dom_container._doms).not_to include(bar)
        expect(dom_container._doms).to include(baz)
      end
    end
  end

  describe "#markup" do
    context "ブロックを渡さない場合" do
      let(:result){ dom_container.markup(receiver_1) }

      it "は、引数をレシーバ群に含むチェーンコンテナを返すこと" do
        expect(result).to be_instance_of(Markup::ChainContainer)
        expect(result._agent.receivers).to match([
          equal(receiver_1),
          equal(receiver_0),
        ])
      end

      context "#_doms" do
        before do
          dom_container << "foo"
          dom_container.markup(receiver_1).hoge
          dom_container << "bar"
        end

        it "は、メソッドチェーンでレシーバに移譲した結果を含むこと" do
          expect(dom_container._doms).to eq([
            "foo",
            "HOGE!",
            "bar",
          ])
        end
      end
    end

    context "ブロックを渡した場合" do
      let(:yieldparam){ dom_container.markup(receiver_1){|m| return m } }

      it "は、引数をレシーバ群に含むDOMコンテナを返すこと" do
        expect(yieldparam).to be_instance_of(Markup::DomContainer)
        expect(yieldparam._agent.receivers).to match([
          equal(receiver_1),
          equal(receiver_0),
        ])
      end

      context "#_doms" do
        before do
          dom_container << "foo"
          dom_container.markup(receiver_1) do |m|
            m.hoge
            m << "てきすと"
            m.piyo
          end
          dom_container << "bar"
        end

        it "は、メソッドチェーンでレシーバに移譲した結果を含むこと" do
          expect(dom_container._doms).to eq([
            "foo",
            "HOGE!てきすとぴよ",
            "bar",
          ])
        end
      end
    end
  end
end
