require 'spec_helper'

describe Markup::TagBuilder do
  let(:agent){ Markup::Agent.new }

  describe "#content_order (private method)" do
    context "インスタンス化時にデフォルト内容を渡した場合" do
      let(:builder){ described_class.new(agent, "default content by argument") }

      context "引数・ブロック無しの場合" do
        it "は、デフォルト内容を返すこと" do
          expect(builder.send(:content_order)).to eq("default content by argument")
        end
      end
    end

    context "インスタンス化時にデフォルト内容・デフォルト内容ブロックを渡した場合" do
      let(:builder){
        described_class.new(agent, "default content by argument") do
          Markup.h.concat("default content by block")
        end
      }

      context "引数・ブロック無しの場合" do
        it "は、デフォルト内容ブロック内で作成した要素群を返すこと" do
          expect(builder.send(:content_order)).to eq("default content by block")
        end
      end

      context "引数に" do
        context "内容を渡した場合" do
          it "は、渡した内容を返すこと" do
            expect(builder.send(:content_order, "content by argument")).to eq("content by argument")
          end
        end

        context "Symbolを渡した場合" do
          let(:agent){ Markup::Agent.new(receiver) }
          let(:receiver){ double("Receiver", shobon: "(´･ω･｀)") }

          it "は、渡したメソッド名をエージェントに移譲した結果を返すこと" do
            expect(builder.send(:content_order, :shobon)).to eq("(´･ω･｀)")
          end
        end

        context "`nil`を渡した場合" do
          it "は、デフォルト内容ブロック内で作成した要素群を返すこと" do
            expect(builder.send(:content_order, nil)).to eq("default content by block")
          end
        end

        context "空文字を渡した場合" do
          it "は、空文字を返すこと" do
            expect(builder.send(:content_order, "")).to eq("")
          end
        end
      end

      context "内容ブロックを渡した場合" do
        subject do
          builder.send(:content_order) do
            Markup.h.concat "content by block"
          end
        end

        it "は、ブロック内で作成した要素群を返すこと" do
          is_expected.to eq("content by block")
        end
      end

      context "引数とブロックを渡した場合" do
        subject do
          builder.send(:content_order, "content by argument") do
            Markup.h.concat "content by block"
          end
        end

        it "は、ブロック内で作成した要素群を返すこと" do
          is_expected.to eq("content by block")
        end
      end
    end
  end

  describe "#tag (private method)" do
    let(:builder){ described_class.new(agent) }

    context "第1引数にタグ名を渡した場合" do
      subject do
        builder.send(:tag, :span)
      end

      it "は、空のタグを生成すること" do
        is_expected.to have_tag("span:empty")
      end
    end

    context "第2引数に内容を渡した場合" do
      subject do
        builder.send(:tag, :span, "(´･ω･｀)")
      end

      it "は、内容を持つタグを生成すること" do
        is_expected.to have_tag("span", text: "(´･ω･｀)")
      end
    end

    context "第2引数に内容・第3引数にHTML属性を渡した場合" do
      subject do
        builder.send(:tag, :span, "(´･ω･｀)", class: "shobon")
      end

      it "は、内容と属性を持つタグを生成すること" do
        is_expected.to have_tag("span.shobon", text: "(´･ω･｀)")
      end
    end

    context "第2引数にHTML属性を渡した場合" do
      subject do
        builder.send(:tag, :span, class: "shobon")
      end

      it "は、属性を持つ空のタグを生成すること" do
        is_expected.to have_tag("span.shobon:empty")
      end
    end

    context "ブロックを渡した場合" do
      subject do
        builder.send(:tag, :span) do
          Markup.h.concat "(・∀・)"
        end
      end

      it "は、ブロック内で作成した要素群を内容として持つタグを生成すること" do
        is_expected.to have_tag("span", text: "(・∀・)")
      end
    end
  end
end
