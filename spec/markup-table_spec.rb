$LOAD_PATH << __dir__.sub(%r{spec$}, "markup-table/lib")
require "markup/table"

Dir.glob("markup-table/spec/**/*_spec.rb") do |path|
  require File.expand_path("../../#{path}", __FILE__)
end
