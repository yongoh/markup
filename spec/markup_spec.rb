require 'spec_helper'

describe Markup do
  let(:owner_class){ Class.new.include(described_class) }
  let(:owner){ owner_class.new }

  let(:receiver){ double("Receiver") }

  describe "::view_context" do
    it "は、ビューのインスタンスを返すこと" do
      expect(described_class.view_context).to be_a(ActionView::Base)
    end

    it "は、ビューヘルパーが使えること" do
      expect(described_class.view_context.content_tag(:span, "(´･ω･｀)", class: "shobon")).to have_tag("span.shobon", text: "(´･ω･｀)")
    end
  end

  describe "#markup" do
    context "ブロックを渡さない場合" do
      it "は、クライアントオブジェクトを返すこと" do
        expect(owner.markup(receiver)).to be_instance_of(Markup::Client).and have_attributes(
          _agent: have_attributes(receivers: [
            equal(receiver),
            be_instance_of(Markup::HtmlBuilder),
            equal(Markup.view_context),
          ]),
        )
      end
    end

    context "ブロックを渡した場合" do
      let(:yieldparam){ owner.markup(receiver){|m| return m } }

      it "は、ブロック内引数にDOMコンテナオブジェクトを渡すこと" do
        expect(yieldparam).to be_instance_of(Markup::DomContainer).and have_attributes(
          _agent: have_attributes(receivers: [
            equal(receiver),
            be_instance_of(Markup::HtmlBuilder),
            equal(Markup.view_context),
          ]),
        )
      end
    end

    shared_examples_for "have merged receivers" do
      it "は、渡したレシーバが最初に来るように基底エージェントとマージしたエージェントを持つこと" do
        expect(container._agent.receivers).to match([equal(receiver), be_instance_of(Markup::HtmlBuilder), equal(Markup.view_context)])
      end
    end

    describe "ブロックを渡さない場合の戻り値" do
      let(:container){ owner.markup(receiver) }
      it_behaves_like "have merged receivers"
    end

    describe "ブロックを渡した場合のブロック内引数" do
      let(:container){ owner.markup(receiver){|m| return m } }
      it_behaves_like "have merged receivers"
    end

    describe "ビルダーが持つエージェント" do
      shared_examples_for "equal agent" do
        let(:builder){ agent.find{|r| r.is_a?(Markup::DomBuilder) } }

        it "は、DOMコンテナのエージェントと同じレシーバ群を持つこと" do
          expect(builder.agent.receivers).to eql(agent.receivers)
        end
      end

      context "ブロックを渡さない場合" do
        let(:agent){ owner.markup(receiver)._agent }
        it_behaves_like "equal agent"
      end

      context "ブロックを渡した場合" do
        let(:agent){ owner.markup(receiver){|m| return m._agent } }
        it_behaves_like "equal agent"
      end

      context "ビルダーメソッドにブロックを渡した場合" do
        let(:agent){ owner.markup(receiver).span{|m| return m._agent } }
        it_behaves_like "equal agent"
      end
    end
  end
end
