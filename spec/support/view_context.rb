RSpec.configure do |config|
  config.include(Module.new do
    def view_context
      @view_context ||= Class.new(ActionView::Base) do
        include ActionView::Helpers
      end.new(ActionView::LookupContext.new(nil), {}, nil)
    end

    alias_method :h, :view_context
  end)
end
