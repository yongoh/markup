# HACK: gem core_ext のパスが何故か通ってないので追加
#   ハッシュ値の並びがバージョンに対応していると仮定。多分間違い。
$LOAD_PATH << Dir.glob("/usr/local/bundle/bundler/gems/core_ext-*/lib").last

require 'action_view'
require 'core_ext'
require 'markup'

Dir.glob(File.join(__dir__, "support", "**", "*.rb")){|f| require f }

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups
  config.filter_run_when_matching :focus
end
