require 'spec_helper'
require 'erb'

describe "Markup::markup" do
  subject do
    ERB.new(source).result(bnd)
  end

  let(:bnd){ Markup.view_context.instance_exec{ return binding } }

  shared_examples_for "render dom" do
    it "は、DOM要素が描画されること" do
      is_expected.to have_tag("div#hoge", text: "ほげ", count: 1)
    end
  end

  shared_examples_for "render no dom" do
    it "は、DOM要素が描画されないこと" do
      is_expected.not_to have_tag("div#hoge")
    end
  end


  context "DOMコンテナのエージェントでDOM要素を生成した場合" do
    context "'<%='の場合" do
      let(:source){
        source = <<EOF
<% a = Markup.markup(self) do |m| %>
  <% m.content_tag(:div, "ほげ", id: "hoge") %>
<% end %>

<%= a %>
EOF
      }

      it_behaves_like "render dom"
    end

    context "'<%'の場合" do
      let(:source){
        source = <<EOF
<% Markup.markup(self) do |m| %>
  <% m.content_tag(:div, "ほげ", id: "hoge") %>
<% end %>
EOF
      }

      it_behaves_like "render no dom"
    end
  end

  context "DOMコンテナにDOM要素を追加した場合" do
    context "'<%='の場合" do
      let(:source){
        source = <<EOF
<% a = Markup.markup(self) do |m| %>
  <% m << content_tag(:div, "ほげ", id: "hoge") %>
<% end %>

<%= a %>
EOF
      }

      it_behaves_like "render dom"
    end

    context "'<%'の場合" do
      let(:source){
        source = <<EOF
<% Markup.markup(self) do |m| %>
  <% m << content_tag(:div, "ほげ", id: "hoge") %>
<% end %>
EOF
      }

      it_behaves_like "render no dom"
    end
  end

  context "`Markup::markup`のブロック内で直接DOM要素を記述した場合" do
    context "'<%='の場合" do
      let(:source){
        source = <<EOF
<% a = Markup.markup(self) do |m| %>
  <div id="hoge">ほげ</div>
<% end %>

<%= a %>
EOF
      }

      it_behaves_like "render dom"
    end

    context "'<%'の場合" do
      let(:source){
        source = <<EOF
<% Markup.markup(self) do |m| %>
  <div id="hoge">ほげ</div>
<% end %>
EOF
      }

      it_behaves_like "render dom"
    end
  end

  context "`Markup::markup`のブロック内でDOM要素を生成した場合" do
    context "'<%='の場合" do
      let(:source){
        source = <<EOF
<% a = Markup.markup(self) do |m| %>
  <%= content_tag(:div, "ほげ", id: "hoge") %>
<% end %>

<%= a %>
EOF
      }

      it_behaves_like "render dom"
    end

    context "'<%'の場合" do
      let(:source){
        source = <<EOF
<% Markup.markup(self) do |m| %>
  <%= content_tag(:div, "ほげ", id: "hoge") %>
<% end %>
EOF
      }

      it_behaves_like "render dom"
    end
  end

  context "`Markup::markup`のブロック内でDOM要素を`concat`した場合" do
    context "'<%='の場合" do
      let(:source){
        source = <<EOF
<% a = Markup.markup(self) do |m| %>
  <% concat content_tag(:div, "ほげ", id: "hoge") %>
<% end %>

<%= a %>
EOF
      }

      it_behaves_like "render dom"
    end

    context "'<%'の場合" do
      let(:source){
        source = <<EOF
<% Markup.markup(self) do |m| %>
  <% concat content_tag(:div, "ほげ", id: "hoge") %>
<% end %>
EOF
      }

      it_behaves_like "render no dom"
    end
  end
end
