$LOAD_PATH << __dir__.sub(%r{spec$}, "markup-list/lib")
require "markup/list"

Dir.glob("markup-list/spec/**/*_spec.rb") do |path|
  require File.expand_path("../../#{path}", __FILE__)
end
