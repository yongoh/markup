require 'spec_helper'

describe Markup::List do
  let(:receiver){ double("Receiver", shobon: "(´･ω･｀)") }

  describe "#markup" do
    let(:owner){ double("Owner").extend(described_class) }
    let(:result){ owner.markup(receiver) }

    shared_examples_for "agent include table builders" do
      it "は、レシーバ群にリスト系ビルダー群を含めること" do
        is_expected.to match([
          be_instance_of(Markup::List::ListBuilder),
          be_instance_of(Markup::List::ItemBuilder),
          be_a(Markup::HtmlBuilder),
          be_a(ActionView::Base),
        ])
      end
    end

    describe "yieldparam._agent" do
      subject{ owner.markup{|m| return m._agent.receivers } }
      it_behaves_like "agent include table builders"
    end

    describe "result._agent" do
      subject{ owner.markup._agent.receivers }
      it_behaves_like "agent include table builders"
    end

    describe ".list" do
      subject do
        result.list do |m|
          m.item(:shobon)
          m.item("(・∀・)")
        end
      end

      example do
        is_expected.to have_tag("ul"){
          with_tag("li", text: "(´･ω･｀)")
          with_tag("li", text: "(・∀・)")
        }
      end
    end

    describe ".item" do
      it "は、リスト項目を生成すること" do
        expect(result.item(:shobon)).to have_tag("li", text: "(´･ω･｀)")
      end
    end
  end
end
