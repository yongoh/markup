require 'spec_helper'

describe Markup::List::ListBuilder do
  let(:builder){ described_class.new(agent) }
  let(:agent){ Markup::Agent.new(receiver) }
  let(:receiver){ double("Receiver", shobon: "(´･ω･｀)") }

  describe "#items" do
    describe "ブロック第1引数" do
      let(:container){ builder.items{|m| return m } }

      it "は、リスト項目ビルダーとリスト項目ラッパーをレシーバとして持つ持つDOMコンテナを渡すこと" do
        expect(container).to be_instance_of(Markup::DomContainer)
        expect(container._agent.receivers).to match([
          be_instance_of(Markup::List::ItemBuilder),
          be_instance_of(Markup::Wrapper).and(have_attributes(_agent: equal(agent))),
        ])
      end

      describe "レシーバが持つメソッド名 + '!'" do
        it "は、レシーバに移譲した結果を内容に持つリスト項目を生成すること" do
          expect(container.shobon!).to have_tag("li", text: "(´･ω･｀)")
        end
      end
    end

    context "ブロックを渡さない場合" do
      it "は、ブロックが無いことを示すエラーを発生させること" do
        expect{ builder.items }.to raise_error(LocalJumpError, "no block given (yield)")
      end
    end

    context "ブロック内でリスト項目群を作成した場合" do
      subject do
        builder.items do |m|
          m.item(:shobon)
          m.shobon
        end
      end

      it "は、リスト項目群を生成すること" do
        is_expected.to have_tag("li", text: "(´･ω･｀)", count: 2)
      end
    end
  end

  describe "#list" do
    subject do
      builder.list do |m|
        m.item("こうもく1")
        m.item("こうもく2", id: "my-id", class: "my-class")
        m.shobon
      end
    end

    it "は、ブロック内で作ったリスト項目要素入りリスト要素を生成すること" do
      is_expected.to have_tag("ul", count: 1){
        with_tag("li", text: "こうもく1", count: 1)
        with_tag("li#my-id.my-class", text: "こうもく2", count: 1)
        with_tag("li", text: "(´･ω･｀)", count: 1)
      }
    end
  end

  describe "#separate" do
    context "引数なしの場合" do
      it "は、デフォルトの内容を持つセパレータ要素を生成すること" do
        expect(builder.separate).to have_tag("span.separator", text: "|")
      end
    end

    context "第1引数に内容を渡した場合" do
      it "は、渡した内容を持つセパレータ要素を生成すること" do
        expect(builder.separate("せぱ")).to have_tag("span.separator", text: "せぱ")
      end
    end
  end
end
