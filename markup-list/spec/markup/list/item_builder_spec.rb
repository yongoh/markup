require 'spec_helper'

describe Markup::List::ItemBuilder do
  let(:builder){ described_class.new(agent) }
  let(:agent){ Markup::Agent.new(receiver) }
  let(:receiver){ double("Receiver", shobon: "(´･ω･｀)") }

  before do
    allow(receiver).to receive(:abc){|a, b, &c| "A=#{a},B=#{b},C=#{c.call}" }
  end

  describe "#item" do
    it "は、リスト項目要素を生成すること" do
      expect(builder.item("こうもく", id: "my-id", class: "my-class")).to have_tag("li#my-id.my-class", text: "こうもく")
    end
  end
end
