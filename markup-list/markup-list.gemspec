$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "markup/list/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "markup-list"
  s.version     = Markup::List::VERSION
  s.authors     = ["yongoh"]
  s.email       = ["a.yongoh@gmail.com"]
  s.homepage    = ""
  s.summary     = ""
  s.description = ""
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency 'markup', Markup::List::VERSION

  s.add_development_dependency "rspec-html-matchers"
end
