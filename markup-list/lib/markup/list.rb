require 'markup/list/item_builder'
require 'markup/list/list_builder'

module Markup
  module List
    include Markup

    private

    def base_receivers
      [ListBuilder, ItemBuilder] + super
    end
  end
end
