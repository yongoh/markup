module Markup
  module List

    # デフォルトのセパレータ
    SEPARATOR = "|".freeze

    class ListBuilder < TagBuilder

      # @param [Hash] default_html_attributes リスト項目要素のHTML属性のデフォルト
      # @yield [m] 内容ブロック（必須）
      # @yieldparam [Markup::DomContainer] m リスト項目ビルダーとリスト項目ラッパーをレシーバとして含むDOMコンテナ
      # @return [ActiveSupport::SafeBuffer] リスト項目群
      def items(**default_html_attributes, &block)
        builder = ItemBuilder.new(agent, nil, default_html_attributes)
        wrapper = Wrapper.new(agent, &builder.method(:item))
        DomContainer.markup(builder, wrapper, &block)
      end

      [
        [:unordered_list, :ul],
        [:ordered_list, :ol],
      ].each do |method_name, tag_name|

        # @return [ActiveSupport::SafeBuffer] リスト要素（<ul> or <ol>）
        define_method(method_name) do |*args, &block|
          tag(tag_name, *args, &block)
        end
      end

      alias_method :list, :unordered_list

      # @return [ActiveSupport::SafeBuffer] セパレータ要素
      def separate(content = SEPARATOR, **html_attributes, &block)
        html_attributes = html_attributes.html_attribute_merge(class: "separator")
        tag(:span, content, html_attributes, &block)
      end

      private

      def tag(*args, &block)
        content_proc = ->(m){ m << items(&block) } if block_given?
        super(*args, &content_proc)
      end
    end
  end
end
