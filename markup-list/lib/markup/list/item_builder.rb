module Markup
  module List
    class ItemBuilder < TagBuilder

      # @return [ActiveSupport::SafeBuffer] リスト項目要素
      # @see TagBuilder#tag
      def item(*args, &block)
        tag(:li, *args, &block)
      end
    end
  end
end
