module Markup
  class ChainContainer < DomContainer

    def initialize(agent, doms)
      super(agent)
      @_doms = doms
    end

    # `@_agent`が持つレシーバが`@_doms`に含まれる場合は、`@_doms`からそのレシーバを削除する
    def _order!(method_name, *args, &block)
      _agent.each{|receiver| _doms.delete(receiver) }
      super(method_name, *args, &block)
    end
  end
end
