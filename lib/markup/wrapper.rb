module Markup
  class Wrapper < Client

    # @yield [result] 移譲結果を処理するブロック
    # @yieldparam [Object] result エージェントに移譲した結果
    # @yieldreturn [Object] `result`を処理した結果
    def initialize(agent, &wrap_proc)
      super(agent)
      @wrap_proc = wrap_proc
    end

    # @return [Object] エージェントに移譲した結果を`@wrap_proc`で処理した結果
    def _order(*args, &block)
      @wrap_proc.call(super(*args, &block))
    end
  end
end
