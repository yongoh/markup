module Markup
  class Agent

    # @!attribute [r] receivers
    #   @return [Array] レシーバの配列
    attr_reader :receivers

    methods = Array.instance_methods - Object.instance_methods - %i(to_hash)
    delegate *methods, to: :receivers

    def initialize(*receivers)
      @receivers = []
      push(*receivers)
    end

    # 追加系メソッド群
    %i(push append << unshift prepend concat).each do |method_name|
      define_method(method_name) do |*receivers|
        @receivers.send(method_name, *init_receivers(receivers))
        @receivers.uniq!
        self
      end
    end

    # 新規作成系メソッド群
    %i(+ -).each do |method_name|
      define_method(method_name) do |other|
        other = other.receivers if other.is_a?(Agent)
        self.class.new(*receivers.send(method_name, other))
      end
    end

    # 渡したメソッド名に応答可能なレシーバ群のうち一番最初のものに移譲する。
    # @param [Symbol,String] method_name 移譲するメソッド名
    # @param [Array] args 移譲する引数群
    # @param [Proc] block 移譲するブロック
    # @return [Onject] 移譲した結果
    # @raise [NoMethodError] レシーバ群の中に渡した名前のメソッドを持つものが無かった場合に発生
    def receive(method_name, *args, &block)
      if receiver = responded_receiver_to(method_name)
        receiver.public_send(method_name, *args, &block)
      else
        raise NoMethodError, "undefined method `#{method_name}' for [#{receiver_inspects(500).join(", ")}]"
      end
    end

    # @param [Symbol,String] method_name メソッド名
    # @return [Hash] 渡したメソッド名に応答可能なレシーバ群
    def responded_receivers_to(method_name)
      receivers.select{|r| r.respond_to?(method_name) }
    end

    # @param [Symbol,String] method_name メソッド名
    # @return [Object] 渡したメソッド名に応答可能なレシーバのうち最初のもの
    def responded_receiver_to(method_name)
      receivers.find{|r| r.respond_to?(method_name) }
    end

    # @param [Symbol,String] method_name メソッド名
    # @return [Boolean] 渡したメソッド名に応答可能なレシーバがあるかどうか
    def receivers_respond_to?(method_name)
      receivers.any?{|r| r.respond_to?(method_name) }
    end

    private

    # @param [Class<Markup::DomBuilder>] builder_class ビルダークラス
    # @return [Markup::DomBuilder] 自身を渡してインスタンス化したビルダー
    def new_builder_from(builder_class)
      builder_class.new(self)
    end

    # @param [Array] receivers 加工前のレシーバ群
    # @return [Array] 加工後のレシーバ群
    def init_receivers(receivers)
      receivers.reduce([]) do |ary, receiver|
        if receiver.is_a?(Agent)
          ary + receiver.receivers
        elsif receiver.is_a?(Class) && receiver <= DomBuilder
          ary << new_builder_from(receiver)
        else
          ary << receiver
        end
      end
    end

    def receiver_inspects(max)
      receivers.map do |receiver|
        inspect = receiver.inspect
        if inspect.length > max
          inspect.slice(0, max) + "..."
        else
          inspect
        end
      end
    end
  end
end
