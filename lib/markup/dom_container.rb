module Markup
  class DomContainer < Client

    class << self

      # @param [Markup::Agent,Object] agent エージェントオブジェクトもしくはレシーバ群
      # @yield [m] 内容を作るブロック
      # @yieldparam [Markup::DomContainer] m 渡したレシーバ群を持つDOMコンテナ
      # @return [ActiveSupport::SafeBuffer] 配下の要素を連結したもの
      # @example ブロック内で`concat`を使って要素を作成
      #   Markup::DomContainer.markup do
      #     h.concat h.content_tag("span", "ほげ")
      #     h.concat "てきすと"
      #     h.concat h.content_tag("div", "ぴよ")
      #   end
      #   #=> "<span>ほげ</span>てきすと<div>ぴよ</div>"
      # @example ERBファイルで使用
      #   <%= Markup::DomContainer.markup do %>
      #     <%= content_tag("span", "ほげ") %>
      #     てきすと
      #     <%= h.content_tag("div", "ぴよ") %>
      #   <% end %>
      #   #=> "<span>ほげ</span>てきすと<div>ぴよ</div>"
      # @example ブロック内でDOMコンテナを使って要素を作成
      #   Markup::DomContainer.markup do |m|
      #     m.span("ほげ")
      #     m << "てきすと"
      #     m.div("ぴよ")
      #   end
      #   #=> "<span>ほげ</span>てきすと<div>ぴよ</div>"
      def markup(*receivers)
        container = new(Agent.new(*receivers))
        Markup.h.capture do
          yield(container)
          container._doms.each{|dom| Markup.h.concat(dom) }
        end
      end
    end

    delegate :<<, to: :_doms

    # @return [Array] DOM群の配列
    def _doms
      @_doms ||= []
    end

    alias_method :_order!, :_order

    # エージェントに移譲した結果を`@_doms`に追加
    # @return [Markup::ChainContainer] メソッドチェーン用DOMコンテナ
    def _order(*args, &block)
      result = _order!(*args, &block)
      _doms << result
      ChainContainer.new(Agent.new(result), _doms)
    end

    # @see Markup::markup
    def markup!(*receivers, &block)
      Markup.markup(*receivers, *_agent.receivers, &block)
    end

    # @return [Markup::ChainContainer] メソッドチェーン用DOMコンテナ
    def markup(*receivers, &block)
      result = markup!(*receivers, &block)
      if block_given?
        _doms << result
        agent = Agent.new(result)
      else
        agent = result._agent
      end
      ChainContainer.new(agent, _doms)
    end

    private

    # メソッドの形で移譲する
    # @example メソッド名で呼び出す
    #   # 下記の2つの式は同じ意味
    #   dom_container._order(:span, "ほげ") #=> "<span>ほげ</span>"
    #   dom_container.span("ほげ")          #=> "<span>ほげ</span>"
    #   # メソッドの結果は`@_doms`に追加される
    #   dom_container._doms #=> ["<span>ほげ</span>", "<span>ほげ</span>"]
    # @example メソッド名に"!"を付けて呼び出す
    #   # 下記の2つの式は同じ意味
    #   dom_container._order!(:span, "ほげ") #=> "<span>ほげ</span>"
    #   dom_container.span!("ほげ")          #=> "<span>ほげ</span>"
    #   # メソッドの結果は`@_doms`に追加されない
    #   dom_container._doms #=> []
    def method_missing(method_name, *args, &block)
      if matched = /\A(\w+)!\z/.match(method_name)
        _order!(matched[1], *args, &block)
      else
        super(method_name, *args, &block)
      end
    end
  end
end
