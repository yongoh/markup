module Markup
  class Client

    # @!attribute [r] _agent
    #   @return [Markup::Agent] エージェントオブジェクト
    attr_reader :_agent

    def initialize(agent)
      @_agent = agent
    end

    # エージェントに移譲
    def _order(*args, &block)
      _agent.receive(*args, &block)
    end

    def respond_to?(method_name)
      super(method_name) || _agent.receivers_respond_to?(method_name)
    end

    private

    # メソッドの形で移譲する
    def method_missing(method_name, *args, &block)
      _order(method_name, *args, &block)
    end
  end
end
