module Markup
  class HtmlBuilder < TagBuilder

    # 使用可能なHTMLタグ
    TAG_NAMES = %i(
      script noscript
      section nav article aside h1 h2 h3 h4 h5 h6 hgroup header footer address
      p hr pre blockquote figure figcaption div main ol ul li dl dt dd
      a em strong small s cite q dfn abbr time code var samp kbd
      sub sup i b mark ruby rt rp bdo span br ins del
      img iframe embed object param video audio source canvas map area
      table col colgroup thead tbody tfoot tr th td
      form fieldset legend label input button select datalist optgroup option
      textarea keygen output progress meter
      details summary command menu
    ).freeze

    # タグ名のメソッド群
    TAG_NAMES.each do |tag_name|
      define_method(tag_name) do |*args, &block|
        tag(tag_name, *args, &block)
      end
    end

    public :tag
  end
end
