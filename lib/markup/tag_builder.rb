module Markup
  class TagBuilder < DomBuilder

    # @!attribute [r] default_content
    #   @return [Object] タグ生成メソッドに内容を渡さなかった場合にタグに入れる内容
    # @!attribute [r] default_html_attributes
    #   @return [Hash] 生成する要素の共通のHTML属性
    #   @note タグ生成メソッドで渡したHTML属性とマージする
    # @!attribute [r] default_content_proc
    #   @return [Proc] タグ生成メソッドに内容を渡さなかった場合に入れる内容を返す関数
    attr_accessor :default_content, :default_html_attributes, :default_content_proc

    # @param [Object] default_content デフォルト内容
    # @param [Hash] default_html_attributes デフォルトHTML属性
    # @yield [m] デフォルト内容ブロック
    def initialize(agent, default_content = nil, default_html_attributes = {}, &default_content_proc)
      super(agent)
      @default_content = default_content
      @default_html_attributes = default_html_attributes
      @default_content_proc = default_content_proc
    end

    private

    # XML要素を生成
    # @param [String,Symbol] tag_name 要素名
    # @param [Hash] html_attributes HTML属性
    # @overload tag(tag_name, content, html_attributes)
    #   第2引数に内容を渡した場合
    #   @param [String,ActiveSupport::SafeBuffer] 要素の内容
    # @overload tag(tag_name, html_attributes){ }
    #   ブロックを渡した場合
    #   @yield [m] 内容ブロック
    #   @yieldparam [Markup::DomContainer] m 自身と同じエージェントを持つDOMコンテナ
    # @return [ActiveSupport::SafeBuffer] 生成した要素
    def tag(tag_name, content = default_content, **html_attributes, &content_proc)
      content = content_order(content, &content_proc)
      html_attributes = default_html_attributes.html_attribute_merge(html_attributes)
      Markup.h.content_tag(tag_name, content, html_attributes)
    end

    # `ActionView::Base#content_tag`に渡す内容の優先付け
    # 優先度：内容ブロック > 引数で渡された内容 > デフォルト内容ブロック > デフォルト内容
    # @param [Object] content 内容
    # @yield [m] 内容ブロック
    # @yieldparam [Markup::DomContainer] m 自身と同じエージェントを持つDOMコンテナ
    # @return [Object] 最も優先される内容
    def content_order(content = nil, &content_proc)
      if block_given?
        markup(&content_proc)
      elsif content
        content_receive(content)
      elsif default_content_proc
        markup(&default_content_proc)
      elsif default_content
        content_receive(default_content)
      end
    end

    def content_receive(content)
      content.is_a?(Symbol) ? agent.receive(content) : content
    end
  end
end
