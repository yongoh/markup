module Markup
  class DomBuilder

    # @!attribute [r] agent
    #   @return [Markup::Agent] エージェントオブジェクト
    attr_reader :agent

    def initialize(agent)
      @agent = agent
    end

    private

    # @see Markup::markup
    def markup(*receivers, &block)
      Markup.markup(*receivers, *agent.receivers, &block)
    end
  end
end
