require "markup/agent"
require "markup/dom_builder"
require "markup/tag_builder"
require "markup/html_builder"
require "markup/client"
require "markup/dom_container"
require "markup/chain_container"
require "markup/wrapper"

module Markup
  class << self

    # @overload markup(*receivers)
    #   @param [Array] receivers レシーバ群
    #   @return [Markup::Client] レシーバ群を持つクライアントオブジェクト
    # @overload markup(*receivers){}
    #   @param [Array] receivers レシーバ群
    #   @return [ActiveSupport::SafeBuffer] ブロック内で作った要素を連結したもの
    #   @see Markup::DomContainer::markup
    def markup(*receivers, &block)
      if block_given?
        DomContainer.markup(*receivers, &block)
      else
        Client.new(Agent.new(*receivers))
      end
    end

    # @return [ActionView::Base] ビューヘルパーメソッド群を持つオブジェクト
    def view_context
      @view_context ||= Class.new(ActionView::Base) do
        include ActionView::Helpers
      end.new(ActionView::LookupContext.new(nil), {}, nil)
    end

    alias_method :h, :view_context
  end

  # @see Markup::markup
  def markup(*receivers, &block)
    Markup.markup(*receivers, *base_receivers, &block)
  end

  alias_method :m, :markup

  private

  def base_receivers
    [HtmlBuilder, Markup.view_context]
  end
end
